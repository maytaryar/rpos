<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSaleDetailsModifierTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sale_details_modifier', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('sale_details_id');
            $table->unsignedBigInteger('item_modifier_id');
            $table->decimal('increment', 18, 2)->default(0);
            $table->timestamps();

            $table->foreign('sale_details_id')
                ->references('id')->on('sale_details')->onDelete('cascade');

            $table->foreign('item_modifier_id')
                ->references('id')->on('modifiers')->onDelete('restrict');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sale_details_modifier');
    }
}
