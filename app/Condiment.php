<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Condiment extends Model
{
    //
    protected $fillable = [
        'item_id', 'kits_id', 'quantity',
    ];


    public function item(){
        $this->belongsTo(Item::class, 'item_id');
    }

    public function items(){
        $this->hasMany(Item::class, 'kits_id', 'id');
    }
}
