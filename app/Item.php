<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Item extends Model
{
    //
    protected $fillable = [
        'name', 'price',
    ];

    protected $casts = [
        'price' => 'float'
    ];
    
    public function condiments()
    {
        return $this->hasMany(Condiment::class);
    }

    public function modifiers(){
        return $this->hasMany(Modifier::class);
    }
}
