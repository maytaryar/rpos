<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Modifier extends Model
{
    //
    protected $fillable = [
        'item_id', 'name', 'increment',
    ];
}
